﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eShopsolution.ViewModels.Catalog.Common
{
    public class PagingRequestPage
    {
        public int pageIndex { get; set; }
        public int pageSize { get; set; }

    }
}
