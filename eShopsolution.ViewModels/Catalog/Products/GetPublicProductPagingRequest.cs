﻿using eShopsolution.ViewModels.Catalog.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace eShopsolution.ViewModels.Catalog.Products
{
    public class GetPublicProductPagingRequest : PagingRequestPage
    {
        public int? CategoryId { get; set; }
    }
}
