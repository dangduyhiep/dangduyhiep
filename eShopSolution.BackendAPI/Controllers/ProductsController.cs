﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eShopsolution.ViewModels.Catalog.ProductImages;
using eShopsolution.ViewModels.Catalog.Products;
using eShopSolution.Application.Catalog.Products;
using eShopSolution.Data.EF;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace eShopSolution.BackendAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {

        private readonly IPublicProductService _publicProductService;
        private readonly IManageProductService _manageProductService;
        public ProductsController(IPublicProductService publicProductService, IManageProductService manageProductService)
        {
            _publicProductService = publicProductService;
            _manageProductService = manageProductService;
        }

        [HttpGet("{languageId}")]
        public async Task<IActionResult> GetAllPaging(string languageId, [FromQuery] GetPublicProductPagingRequest request)
        {
            var products = await _publicProductService.GetAllByCategoryId(languageId , request);
            return Ok(products);
        }

        [HttpGet("{productId}/{languageId}")]
        public async Task<IActionResult> GetById(int productId , string languageId)
        {
            var product = await _manageProductService.GetById(productId, languageId);
            if(product == null)
            {
                return BadRequest("Can not find product");
            }
            return Ok(product);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromForm] ProductCreateRequest request)
        {
            if(ModelState.IsValid ==  false)
            {
                return BadRequest(ModelState);
            }
            var productId = await _manageProductService.Create(request);
            if(productId == 0)
            {
                return BadRequest();
            }
            var product = await _manageProductService.GetById(productId, request.LanguageId);
            return CreatedAtAction(nameof(GetById),new { id = productId}, productId);
        }

        [HttpPut]
        public async Task<IActionResult> PutUpdate([FromForm] ProductUpdateRequest request)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }
            var productId = await _manageProductService.Update(request);
            if (productId == 0)
            {
                return BadRequest();
            }
            return Ok();
        }

        [HttpDelete("{productId}")]
        public async Task<IActionResult> Delete(int productId)
        {
            var product = await _manageProductService.Delete(productId);
            if(product == 0)
            {
                return BadRequest("can not find product");
            }
            return Ok();
        }

        [HttpPatch("{id}/{newPrice}")]
        public async Task<IActionResult> PutUpdatePrice(int id , decimal newPrice)
        {
            var isSuccessful = await _manageProductService.UpdatePrice(id,newPrice);
            if (isSuccessful)
            {
                return Ok();
            }
            return BadRequest();
        }

        //images
        [HttpPost("{productId}/Images")]
        public async Task<IActionResult> Create(int productId ,[FromForm] ProductImageCreateRequest request)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }
            var productImageId = await _manageProductService.AddImage(productId,request);
            if (productImageId == 0)
            {
                return BadRequest();
            }
            var image = await _manageProductService.GetImageById(productImageId);
            return CreatedAtAction(nameof(GetImageById), new { id = productImageId }, image);
        }

        //images
        [HttpPut("{productId}/Images/{imageId}")]
        public async Task<IActionResult> UpdateImg(int imageId, [FromForm] ProductImageUpdateRequest request)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }
            var productImageId = await _manageProductService.UpdateImage(imageId, request);
            if (productImageId == 0)
            {
                return BadRequest();
            }
            return Ok();
        }
        [HttpDelete("{productId}/Images/{imageId}")]
        public async Task<IActionResult> RemoveImg(int imageId)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }
            var productImageId = await _manageProductService.RemoveImage(imageId);
            if (productImageId == 0)
            {
                return BadRequest();
            }
            return Ok();
        }

        [HttpGet("{productId}/images/{imageId}")]
        public async Task<IActionResult> GetImageById(int imageId)
        {
            var image = await _manageProductService.GetImageById(imageId);
            if (image == null)
            {
                return BadRequest("Can not find product");
            }

            return Ok(image);
        }
    }
}
