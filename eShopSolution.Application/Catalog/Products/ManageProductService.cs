﻿using System;
using System.Collections.Generic;
using System.Text;
using eShopSolution.Data.EF;
using System.Threading.Tasks;
using eShopSolution.Data.Entities;
using eShopSolution.Utilities.Exceptions;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using eShopsolution.ViewModels.Catalog.Common;
using eShopsolution.ViewModels.Catalog.Products;
using Microsoft.AspNetCore.Http;
using System.Net.Http.Headers;
using System.IO;
using eShopSolution.Application.common;
using Microsoft.AspNetCore.Mvc;
using eShopsolution.ViewModels.Catalog.ProductImages;
using System.Security.Cryptography.X509Certificates;

namespace eShopSolution.Application.Catalog.Products
{
    public class ManageProductService : IManageProductService
    {
        private readonly eShopDBContext _Context;   
        private readonly IStorageService _storageService;
        public ManageProductService(eShopDBContext Context, IStorageService storageService)
        {
            _Context = Context;
            _storageService = storageService;
        }

        public async Task<int> AddImage(int productId, ProductImageCreateRequest request)
        {
            var productImage = new ProductImage
            {
                Caption = request.Caption,
                DateCreated = DateTime.Now,
                IsDefault = request.IsDefault,
                ProductId = productId,
                SortOrder = request.SortOrder
            };
            if (request.ImageFile != null)
            {
                productImage.ImagePath = await this.SaveFile(request.ImageFile);
                productImage.FileSize = request.ImageFile.Length;             
            }
            _Context.ProductImages.Add(productImage);
             await _Context.SaveChangesAsync();
            return productImage.Id;
        }

        public async Task AddViewCount(int productId)
        {
            var product = await _Context.Products.FindAsync(productId);
            product.ViewCount += 1;
            await _Context.SaveChangesAsync();

        }

        public async Task<int> Create(ProductCreateRequest request)
        {
            var product = new Product()
            {            
                Price = request.Price,
                OriginalPrice = request.OriginalPrice,
                Stock = request.Stock,
                ViewCount = 0,
                DateCreated = DateTime.Now,
                ProductTranslations = new List<ProductTranslation>
                {
                    new ProductTranslation
                    {
                        Name = request.Name,
                        Description = request.Description,
                        Details = request.Details,
                        SeoDescription = request.SeoDescription,
                        SeoAlias = request.SeoAlias,
                        SeoTitle = request.SeoTitle,
                        LanguageId = request.LanguageId
                    }
                }
            };
            //saveimage
            if (request.ThumbnailImage != null)
            {
                product.ProductImages = new List<ProductImage>()
                {
                    new ProductImage()
                    {
                        Caption = "Thumbnail image",
                        DateCreated = DateTime.Now,
                        FileSize = request.ThumbnailImage.Length,
                        ImagePath = await this.SaveFile(request.ThumbnailImage),
                        IsDefault = true,
                        SortOrder = 1
                    }
                };
            }
            _Context.Products.Add(product);
            await _Context.SaveChangesAsync();
            return product.Id;

        }

        public async Task<int> Delete(int productId)
        {
            var product = await _Context.Products.FindAsync(productId);
            if (product == null)
            {
                throw new eShopException($"Cannot find product : {productId}");
            }          
            var images = _Context.ProductImages.Where(x => x.ProductId == productId);
            foreach (var image in images)
            {
               await _storageService.DeleteFileAsync(image.ImagePath);
            }
            _Context.Products.Remove(product);
            return await _Context.SaveChangesAsync();
        }


        public async Task<PageResult<ProductViewModel>> GetAllPaging(GetManageProductPagingRequest request)
        {
            // Select
            var query = from p in _Context.Products
                        join pt in _Context.ProductTranslations on p.Id equals pt.ProductId
                        join pic in _Context.ProductInCategories on p.Id equals pic.ProductId
                        join ct in _Context.Categories on pic.ProductId equals ct.Id
                        where pt.Name.Contains(request.Keyword)
                        select new { p, pt, pic };
            //Fillter
            if (!string.IsNullOrEmpty(request.Keyword))
                query = query.Where(x => x.pt.Name.Contains(request.Keyword));
            if (request.CategoryIds.Count > 0)
            {
                query = query.Where(x => request.CategoryIds.Contains(x.pic.CategoryId));
            }
            //paging
            int totalRow = await query.CountAsync();

            var data = await query.Skip((request.pageIndex - 1) * request.pageSize)
                            .Take(request.pageSize)
                            .Select(x => new ProductViewModel() {
                                Id = x.p.Id,
                                Name = x.pt.Name,
                                DateCreated = x.p.DateCreated,
                                Description = x.pt.Description,
                                Details = x.pt.Details,
                                LanguageId = x.pt.LanguageId,
                                OriginalPrice = x.p.OriginalPrice,
                                Price = x.p.Price,
                                SeoAlias = x.pt.SeoAlias,
                                SeoDescription = x.pt.SeoDescription,
                                SeoTitle = x.pt.SeoTitle,
                                ViewCount = x.p.ViewCount,
                            }).ToListAsync();
            //select and project
            var pagedResult = new PageResult<ProductViewModel>()
            {
                TotalRecord = totalRow,
                Items = data,
            };
            return pagedResult;
        }

        public async Task<ProductViewModel> GetById(int productId, string languageId)
        {
            var product = await _Context.Products.FindAsync(productId);
            var productTranslation = await _Context.ProductTranslations
                .FirstOrDefaultAsync(x => x.ProductId == productId && x.LanguageId == languageId);
            var productViewModle = new ProductViewModel()
            {
                Id = product.Id,
                DateCreated = product.DateCreated,
                Description = productTranslation != null ? productTranslation.Description : null,
                LanguageId = productTranslation.LanguageId,
                Details = productTranslation != null ? productTranslation.Details : null,
                Name = productTranslation != null ? productTranslation.Name : null,
                OriginalPrice = product.OriginalPrice,
                Price = product.Price,
                SeoAlias = productTranslation != null ? productTranslation.SeoAlias : null,
                SeoDescription = productTranslation != null ? productTranslation.SeoDescription : null,
                SeoTitle = productTranslation != null ? productTranslation.SeoTitle : null,
                Stock = product.Stock,
                ViewCount = product.ViewCount,
            };
            return productViewModle;

        }

        public async Task<ProductImageViewModel> GetImageById(int imageId)
        {
            var image = await _Context.ProductImages.FindAsync(imageId);
            if (image == null)
            {
                throw new eShopException($"Cannot find product : {imageId}");
            }
            var viewModel = new ProductImageViewModel()
            {
                Caption = image.Caption,
                DateCreated = image.DateCreated,
                FileSize = image.FileSize,
                Id = image.Id,
                IsDefault = image.IsDefault,
                ProductId = image.ProductId,
                SortOrder = image.SortOrder
            };
            return viewModel;
        }

        public async Task<List<ProductImageViewModel>> GetListImage(int productId)
        {
            return await _Context.ProductImages.Where(x => x.ProductId == productId)
                                  .Select(i => new ProductImageViewModel() 
                                  { 
                                       Caption = i.Caption,
                                       DateCreated = i.DateCreated,
                                       FileSize = i.FileSize,
                                       Id = i.Id,
                                       IsDefault = i.IsDefault,
                                       ProductId = productId,
                                       SortOrder = i.SortOrder
                                  }).ToListAsync(); 
        }

        public async Task<int> RemoveImage(int imageId)
        {
            var productImage = await _Context.ProductImages.FindAsync(imageId);
            if (productImage == null)
            {
                throw new eShopException($"Cannot find product : {imageId}");
            }
            _Context.ProductImages.Remove(productImage);
            return await _Context.SaveChangesAsync();
        }

        public async Task<int> Update(ProductUpdateRequest request)
        {
            var product = await _Context.Products.FindAsync(request.Id);
            var productStraslation =await _Context.ProductTranslations
                                              .FirstOrDefaultAsync(x => x.ProductId == request.Id 
                                                                    && x.LanguageId == request.LanguageId);

            if (product == null || productStraslation == null) throw new eShopException($"Cannot find product : {request.Id}");

            productStraslation.Name = request.Name;
            productStraslation.SeoAlias = request.SeoAlias;
            productStraslation.SeoDescription = request.SeoDescription;
            productStraslation.SeoTitle = request.SeoTitle;
            productStraslation.Details = request.Details;

            //Save File
            if (request.ThumbnailImage != null)
            {
                var thumbnailImage = await _Context.ProductImages
                                    .FirstOrDefaultAsync(x => x.IsDefault == true && x.ProductId == request.Id);
                if(thumbnailImage != null)
                {
                    thumbnailImage.FileSize = request.ThumbnailImage.Length;
                    thumbnailImage.ImagePath = await this.SaveFile(request.ThumbnailImage);
                    _Context.ProductImages.Update(thumbnailImage);
                }
            }
            return await _Context.SaveChangesAsync();

        }

        public async Task<int> UpdateImage(int imageId, ProductImageUpdateRequest request)
        {
            var productImage = await _Context.ProductImages.FindAsync(imageId);
            if(productImage == null)
            {
                throw new eShopException($"Cannot find an image with id {imageId}");
            }    
            if (request.ImageFile != null)
            {
                productImage.ImagePath = await this.SaveFile(request.ImageFile);
                productImage.FileSize = request.ImageFile.Length;
            }
            _Context.ProductImages.Update(productImage);
            return await _Context.SaveChangesAsync();
        }

        public async Task<bool> UpdatePrice(int productId, decimal newPrice)
        {
            var product = await _Context.Products.FindAsync(productId);

            if (product == null)
                throw new eShopException($"Cannot find product : {productId}");
            product.Price = newPrice;
            
            return await _Context.SaveChangesAsync() > 0;
        }

        public async Task<bool> UpdateStock(int productId, int addQuantity)
        {
            var product = await _Context.Products.FindAsync(productId);

            if (product == null)
                throw new eShopException($"Cannot find product : {productId}");
            product.Stock += addQuantity;

            return await _Context.SaveChangesAsync() > 0;
        }

        private async Task<string> SaveFile(IFormFile file)
        {
            var originalFileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
            var fileName = $"{Guid.NewGuid()}{Path.GetExtension(originalFileName)}";
            await _storageService.SaveFileAsync(file.OpenReadStream(), fileName);
            return fileName;
        }

    }
}
